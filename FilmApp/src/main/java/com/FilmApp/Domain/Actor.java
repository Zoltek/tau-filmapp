package com.FilmApp.Domain;

import org.springframework.data.annotation.Id;

import javax.persistence.Entity;
import java.time.LocalDate;

//@Entity
public class Actor {

    private int id;
    private String name;
    private String surname;
    private LocalDate createTime;
    private LocalDate updateTime;
    private LocalDate readTime;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Id
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDate createTime) {
        this.createTime = createTime;
    }

    public LocalDate getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDate updateTime) {
        this.updateTime = updateTime;
    }

    public LocalDate getReadTime() {
        return readTime;
    }

    public void setReadTime(LocalDate readTime) {
        this.readTime = readTime;
    }
}

package com.FilmApp.Domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDate;
import java.util.List;

@Entity
public class Film {

    private int id;
    private String title;
    private String description;
//    private List<Actor> cast;
    private LocalDate createTime;
    private LocalDate updateTime;
    private LocalDate readTime;

    public Film(int id, String title, String description) {
        this.id = id;
        this.title = title;
        this.description = description;
    }

    public Film(){}

    @Id
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
//
//    public List<Actor> getCast() {
//        return cast;
//    }
//
//    public void setCast(List<Actor> cast) {
//        this.cast = cast;
//    }

    public LocalDate getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDate createTime) {
        this.createTime = createTime;
    }

    public LocalDate getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDate updateTime) {
        this.updateTime = updateTime;
    }

    public LocalDate getReadTime() {
        return readTime;
    }

    public void setReadTime(LocalDate readTime) {
        this.readTime = readTime;
    }
}

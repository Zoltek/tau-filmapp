package com.FilmApp.Services;

import com.FilmApp.Domain.Actor;
import com.FilmApp.Domain.Film;

import java.util.List;

public interface IFilmService {

    Film getFilm(int id);
    void clear();
    List<Film> getAllFilms();
    List<Actor> getAllActors();
    boolean addActor(int id, String name, String surname);
    Actor getActor(int id);
    void deleteFilm(int filmId);
    boolean deleteActor(int actorId);
    void updateFilm(int id, String title, String description);
    boolean updateActor(int id, String title, String description);
    void addFilm(int id, String title, String description);
    void updateTimeActor(Actor actor, TimeUpdateState state);
    void switchCreateTime(boolean state);
    void switchUpdateTime(boolean state);
    void switchReadTime(boolean state);
    Film findByTitleLike(String title);

}

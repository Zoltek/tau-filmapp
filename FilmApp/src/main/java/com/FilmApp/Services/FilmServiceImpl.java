package com.FilmApp.Services;

import com.FilmApp.Domain.Actor;
import com.FilmApp.Domain.Film;
import com.FilmApp.Repository.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;

@Service
public class FilmServiceImpl implements IFilmService{

    @Autowired
    FilmRepository db;

    private boolean createTime = true;
    private boolean updateTime = true;
    private boolean readTime = true;

//    private List<Film> db = new ArrayList<>();
    private List<Actor> dba = new ArrayList<>();


    public Film getFilm(int id){

        return db.getOne(id);
    }

    public List<Film> getAllFilms() {
        return db.findAll();
    }

    public List<Actor> getAllActors() {
        for(Actor a: dba){
            updateTimeActor(a, TimeUpdateState.READ);
        }
        return dba;
    }

    public Actor getActor(int id){

        for(Actor a: dba){
            if (a.getId()==id){
                updateTimeActor(a, TimeUpdateState.READ);
                return a;
            }
        }
        throw new IllegalArgumentException("Actor does not exist");
    }

    public void deleteFilm(int filmId) {
        db.delete(db.getOne(filmId));
    }

    public boolean deleteActor(int actorId) {
        dba.remove(actorId);
        return true;
    }

    public void updateFilm(int id, String title, String description){
        Film f = db.getOne(id);
        f.setTitle(title);
        f.setDescription(description);
        db.save(f);
    }

    public boolean updateActor(int id, String title, String description){
        for (Actor a: dba) {
            if (a.getId() == id) {
                a.setName(title);
                a.setSurname(description);
                updateTimeActor(a, TimeUpdateState.UPDATE);
                return true;
            }
        }
        throw new IllegalArgumentException("Cant update film");
    }

    public void addFilm(int id, String title, String description){
        if(db.getOne(id) != null){
            db.save(new Film(id, title, description));
        }else {
            Film f = db.getOne(id);
            f.setTitle(title);
            f.setDescription(description);
            db.save(f);
        }
    }

    public void clear(){
        db.deleteAll();
    }

    public boolean addActor(int id, String name, String surname){
        Actor actor = new Actor();

        for(Actor a: dba){
            if(a.getId() == id){
                throw new IllegalArgumentException("Actor with this ID exist");
            }
        }
        actor.setId(id);
        actor.setName(name);
        actor.setSurname(surname);
        updateTimeActor(actor, TimeUpdateState.CREATE);
        dba.add(actor);
        return true;
    }

    public void updateTimeActor(Actor actor, TimeUpdateState state){
        if(state == TimeUpdateState.CREATE && createTime){
            for (Actor a: dba){
                if (a.getId() == actor.getId()) {
                    a.setCreateTime(LocalDateTime.now().toLocalDate());
                }
            }
        }
        if(state == TimeUpdateState.CREATE && updateTime){
            for (Actor a: dba){
                if (a.getId() == actor.getId()){
                    a.setCreateTime(LocalDateTime.now().toLocalDate());
                }
            }
        }
        if(state == TimeUpdateState.UPDATE && updateTime) {
            for (Actor a : dba) {
                if (a.getId() == actor.getId()) {
                    a.setUpdateTime(LocalDateTime.now().toLocalDate());
                }
            }
        }
        if(state == TimeUpdateState.READ && readTime){
            for (Actor a: dba){
                if (a.getId() == actor.getId()) {
                    a.setReadTime(LocalDateTime.now().toLocalDate());
                }
            }
        }
    }

    public void switchCreateTime(boolean state){
        this.createTime = state;
    }

    public void switchUpdateTime(boolean state){
        this.updateTime = state;
    }

    public void switchReadTime(boolean state){
        this.readTime = state;
    }

    @Override
    public Film findByTitleLike(String filmTitle) {
        return db.findByTitleLike(filmTitle);
    }
}

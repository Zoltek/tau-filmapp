package com.FilmApp.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class CommandRunner implements CommandLineRunner {

    @Autowired
    IFilmService filmService;

    @Override
    public void run(String... args) throws Exception {
        filmService.clear();
        filmService.addFilm(0, "Przeminelo z Wiatrem", "Byl taki film");
        filmService.addFilm(1, "Pamietniki z wakacji", "Miesny jez");
        filmService.addFilm(2, "Smolensk", "Film niskich lotow");
        filmService.addFilm(3, "Smolenskowe Przygody", "Film niskich lotow");
        filmService.addFilm(4, "Cudawianki", "Film niskich lotow");
        filmService.addFilm(5, "Halynka podaj piwo", "Film niskich lotow");
    }
}

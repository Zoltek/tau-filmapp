package com.FilmApp.Controller;

import com.FilmApp.Domain.Film;
import com.FilmApp.Services.FilmServiceImpl;
import com.FilmApp.Services.IFilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Entity;
import java.util.List;

@RestController
public class FilmController {

    @Autowired
    FilmServiceImpl filmService;

    @GetMapping
    public List<Film> getAll(){
        return filmService.getAllFilms();
    }

    @PostMapping
    public void add(@RequestBody Film film){
        System.out.println(film.toString());
        filmService.addFilm(film.getId(), film.getTitle(), film.getDescription());
    }

    @DeleteMapping("api/filmapp/{id}")
    public void delete(@PathVariable("id") int id){
        filmService.deleteFilm(id);
    }

    @PutMapping("/api/filmapp/{id}")
    public void update(@PathVariable("id") int id, @RequestBody Film film){
        filmService.updateFilm(id, film.getTitle(), film.getDescription());
    }
}

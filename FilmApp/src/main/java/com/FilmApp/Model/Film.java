package com.FilmApp.Model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Film {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String film;
    private String description;
    @CreatedDate
    private Date createTime;
    @LastModifiedDate
    private Date modifyTime;

    private Date readTime;

    @Override
    public String toString(){
        return "Film{" +
                "id=" + id +
                ", film='" + film + '\'' +
                ", description='" + description + '\'' +
                ", createTime=" + createTime +
                ", modifyTime=" + modifyTime +
                ", readTime=" + readTime +
                '}';
    }

    public Film(String film, String description){
        this.film = film;
        this.description = description;
    }
}

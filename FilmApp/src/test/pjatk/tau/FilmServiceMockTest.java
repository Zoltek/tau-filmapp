package pjatk.tau;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import com.FilmApp.Services.FilmServiceImpl;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FilmServiceMockTest {

    @InjectMocks
    FilmServiceImpl filmService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

    }

    @Before
    public void setDataBase(){
        filmService.addFilm(0, "M jak milosc", "Byl taki film");
        filmService.addFilm(1, "Klan", "Miesny jez");
        filmService.addFilm(2, "Na wspolnej", "Film niskich lotow");

        filmService.addActor(0, "Andrzej", "Wajda");
        filmService.addActor(1, "Tomasz", "Karolak");
        filmService.addActor(2, "Piotr", "Adamczyk");
    }

    @Test
    public void checkCreateTimeOfActor(){
        when(filmService.addActor(3, "Mock", "Mockowski")).thenReturn(true);
        filmService.addActor(3, "Mock", "Mockowski");
        verify(filmService, times(1)).getActor(3);
    }

    @Test
    public void addingValueToDatabase(){
        when(filmService.addActor(3, "Mock", "Mockowski")).thenReturn(true);
        assertNotNull("Film not exist", filmService.getFilm(3));
        verify(filmService, times(1)).getActor(3);
    }

    @Test(expected = RuntimeException.class)
    public void addingValueToDatabase_throwsExcepion(){
        when(filmService.addActor(anyInt(), anyString(), anyString())).thenThrow(RuntimeException.class);
        filmService.addActor(3, "Mock", "Mockowski");
        verify(filmService).addActor(4, "Mock2", "Mockowski2");
    }

    @Test
    public void checkReadDateFromGameObject(){
        when(filmService.getActor(1)).thenReturn(Optional.ofNullable());
        Optional<filmService> actor = filmService.getActor(1);

        assertNotEquals(actor.getReadTime(), LocalDateTime.now().toLocalDate());

        verify(filmService).getActor(1);
    }

    @Test
    public void checkModifyDateFromGameObject(){
        FilmServiceImpl film = mock(FilmServiceImpl.class);
        when(filmService.getActor(1)).thenReturn(Optional.ofNullable(film));
        assertEquals(filmService.getActor(1).getReadTime(), film.getReadTime());
    }

    @Test
    public void getObjectDataInformations(){
        FilmServiceImpl film = mock(FilmServiceImpl.class);
        when(filmService.getActor(1)).thenReturn(Optional.ofNullable(film));
        filmService.getActor(1);

        assertEquals(filmService.getActor(1).getReadTime(), film.getReadTime());
        assertEquals(filmService.getActor(1).getUpdateTime(), film.getUpdateTime());
        assertEquals(filmService.getActor(1).getCreateTime(), film.getCreateTime());
    }


    @After
    public void clearDataBase(){
        filmService.clear();
    }
}
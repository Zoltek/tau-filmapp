package pjatk.tau;

import com.FilmApp.Services.FilmServiceImpl;
import com.FilmApp.Services.IFilmService;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class FilmServiceTest {

    private static IFilmService filmService;

    @BeforeClass
    public static void initializeObject(){
        filmService = new FilmServiceImpl();
    }

    @Before
    public void prepareTest(){
        filmService.addFilm(0, "Przeminelo z Wiatrem", "Byl taki film");
        filmService.addFilm(1, "Pamietniki z wakacji", "Miesny jez");
        filmService.addFilm(2, "Smolensk", "Film niskich lotow");

        filmService.addActor(0, "Lech", "Kaczynski");
        filmService.addActor(1, "Jarosław", "Kaczynski");
        filmService.addActor(2, "Maria", "Kaczynska");
    }

    @Test
    public void databaseIsInitialized(){
        assertNotNull("Database doesnt not initialized", filmService);
    }

    @Test
    public void checkFilmDatabaseHas3Elements(){
        assertEquals("Film DB has not 3 elements", 3, filmService.getAllFilms().size());
    }
//
//    @Test
//    public void checkUpdateFilm(){
//        assertTrue("Can not update film", filmService.updateFilm(1, "Szkola", "Kanapka"));
//    }

//    @Test(expected = IllegalArgumentException.class)
//    public void checkUpdateWhenFilmDoesNotExist(){
//        assertTrue("Cant update film", filmService.updateFilm(4, "Szkola", "Kanapka"));
//    }

    @Test(expected = IllegalArgumentException.class)
    public void checkGetNotExistFilm(){
        assertNotNull("Film not exist", filmService.getFilm(5));
    }

//    @Test(expected = IllegalArgumentException.class)
//    public void checkDeleteNotExistFilm(){
//        assertTrue("Film not exist", filmService.deleteFilm(5));
//    }

//    @Test
//    public void checkAddFilm(){
//        assertTrue("Cant add film", filmService.addFilm(4, "New Film", "Some description"));
//        assertNotNull("Cant get new added film", filmService.getFilm(4));
//    }

//    @Test(expected = IllegalArgumentException.class)
//    public void checkAddFilmOnExistID(){
//        assertTrue("Cant add film", filmService.addFilm(0, "New Film", "Some description"));
//    }



    @Test(expected = IllegalArgumentException.class)
    public void checkGetNotExistActor(){
        assertNotNull("Film not exist", filmService.getActor(5));
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkUpdateWhenActorDoesNotExist(){
        assertTrue("Cant update film", filmService.updateActor(5, "Budyn", "Developer"));
    }

    @Test
    public void checkUpdateActor(){
        assertTrue("Cant update film", filmService.updateActor(1, "Budyn", "Developer"));
    }



    @Test(expected = IllegalArgumentException.class)
    public void checkDeleteNotExistActor(){
        assertTrue("Film not exist", filmService.deleteActor(5));
    }

    @Test
    public void checkDeleteActor(){
        assertTrue("Cant delete Actor", filmService.deleteActor(1));
    }

    @Test
    public void checkAddActor(){
        assertTrue("Cant add film", filmService.addActor(4, "Jan Maria", "Rokita"));
        assertNotNull("Cant get new added film", filmService.getActor(4));
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkAddActorOnExistID(){
        assertTrue("Cant add Actor", filmService.addActor(1, "Jan Maria", "Rokita"));
    }

    @Test
    public void checkClearDB(){
        filmService.clear();
        assertEquals("Film DB has not 3 elements", 0, filmService.getAllFilms().size());
        assertEquals("Film DB has not 3 elements", 0, filmService.getAllActors().size());
    }

    @Test
    public void checkActorDatabaseHas3Elements(){
        assertEquals("Actor DB has not 3 elements", 3, filmService.getAllActors().size());
    }

    @After
    public void clearAferTest(){
        filmService.clear();
    }
}

Feature: Film Service
  This test show how to use this app

  Scenario: Film database is not empty
    Given we have filmservice
    When try add some film
    Then db is not empty


  Scenario Outline: First test with regex
    Given we have filmservice
    And add some film to db <a>
    When db should by not empty
    Then the number of film should be <b>

    Examples:
      | a | b |
      | 4 | 1 |
      | 5 | 1 |
      | 6 | 1 |

  Scenario Outline: Remove some films
    Given we have filmservice
    And add some film to db <a>
    But db should by not empty
    When delete some film <a>
    Then the number of film should be 0

    Examples:
      | a |
      | 1 |
      | 2 |
      | 3 |
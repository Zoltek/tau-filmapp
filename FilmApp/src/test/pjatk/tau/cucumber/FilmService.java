package pjatk.tau.cucumber;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import com.FilmApp.Services.FilmServiceImpl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class FilmService {

    private FilmServiceImpl filmService;

    @Given("^we have filmservice$")
    public void we_have_filmservice(){
        filmService = new FilmServiceImpl();
    }

    @Given("^add some film to db (\\d+)$")
    public void add_some_film_to_db(int arg1){

        filmService.addFilm(arg1, "Film", "jakis opis");
    }

    @When("^try add some film$")
    public void try_add_some_film(){
        filmService.addFilm(0, "Przeminelo z Wiatrem", "Byl taki film");
        filmService.addFilm(1, "Pamietniki z wakacji", "Miesny jez");
        filmService.addFilm(2, "Smolensk", "Film niskich lotow");
    }

    @When("^db should by not empty$")
    public void db_should_by_not_empty(){
        assertNotNull(filmService.getAllFilms());
    }

    @When("^delete some film (\\d+)$")
    public void delete_some_film(int arg1){
        filmService.deleteFilm(arg1);
    }

    @Then("^db is not empty$")
    public void db_is_not_empty(){
        assertNotNull(filmService.getAllFilms());
    }

    @Then("^the number of film should be (\\d+)$")
    public void the_number_of_film_should_be(int arg1){
        assertEquals(arg1, filmService.getAllFilms().size());
    }





}

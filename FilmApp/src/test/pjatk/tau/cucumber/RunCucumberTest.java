package pjatk.tau.cucumber;


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty","html:report"},
                features = {"FilmApp\\src\\test\\pjatk\\tau\\cucumber\\"})
public class RunCucumberTest {
}
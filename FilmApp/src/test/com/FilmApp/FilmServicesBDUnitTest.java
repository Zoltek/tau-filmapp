package com.FilmApp;

import com.FilmApp.Services.FilmServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Rollback
public class FilmServicesBDUnitTest {

    @Autowired
    FilmServiceImpl filmService;

    @Test
    public void getDatabaseSize(){
        assertEquals(6, filmService.getAllFilms().size());
        filmService.addFilm(7,"Test", "DBUnitTest");
        assertEquals(7, filmService.getAllFilms().size());
    }

    @Test
    public void removeOneRecord(){
        assertEquals(6, filmService.getAllFilms().size());
        filmService.deleteFilm(5);
        assertEquals(5,filmService.getAllFilms().size());
    }

    @Test
    public void updateFirstRecord(){
        assertEquals("Przeminelo z Wiatrem", filmService.getFilm(1).getTitle());
        filmService.updateFilm(1, "DBUnit", "test");
        assertEquals("DBUnit", filmService.getFilm(1).getTitle());
    }

    @Test
    public void getOneRecord(){
        assertEquals("Smolensk", filmService.getFilm(3).getTitle());
    }

    @Test
    public void findByFilmName(){
        assertNotNull(filmService.findByTitleLike("Smolensk"));
    }
}

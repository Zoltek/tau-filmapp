//package com.FilmApp;
//
//import java.io.FileOutputStream;
//import java.sql.Connection;
//import java.sql.DriverManager;
//
//import org.dbunit.database.DatabaseConnection;
//import org.dbunit.database.IDatabaseConnection;
//import org.dbunit.dataset.IDataSet;
//import org.dbunit.dataset.xml.FlatDtdDataSet;
//import org.dbunit.dataset.xml.FlatXmlDataSet;
//
//// mvn exec:java -Dexec.mainClass="com.FilmApp.DatabaseExport" -Dexec.classpathScope=test
//
//public class DatabaseExport {
//    public static void main(String[] args) throws Exception {
//        Connection jdbcConnection = DriverManager.getConnection(
//                "jdbc:postgresql://localhost:5432/tau", "postgres", "admin");
//
//        IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);
//
//        FlatDtdDataSet.write(connection.createDataSet(), new FileOutputStream(
//                "src/test/resources/databaseDump.dtd"));
//
//        FlatXmlDataSet.write(connection.createDataSet(), new FileOutputStream(
//                "src/test/resources/databaseDump.xml"));
//    }
//
//}
